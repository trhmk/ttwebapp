const client = require('soundoftext-js');

client.sounds.create({ text:'ပြည်ထောင်စု မြန်မာနိုင်ငံတော်', voice: 'my' })
  .then(soundUrl => {
    console.log(soundUrl); // https://soundoftext.nyc3.digitaloceanspaces.com/<sound-id>.mp3
  })
  .catch(e => {
    /* Reasons that the Promise might get rejected:
     * - after 60 seconds, it automatically times out
     * - the API might fail to create the sound or reject it
     * - other miscellaneous network issues
     */
  });