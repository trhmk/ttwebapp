// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
      apiKey: "AIzaSyASBFEE5DTIIi-zGLLTfndBFZJvg2nyxc8",
      authDomain: "mmocr-254204.firebaseapp.com",
      databaseURL: "https://mmocr-254204.firebaseio.com",
      projectId: "mmocr-254204",
      storageBucket: "mmocr-254204.appspot.com",
      messagingSenderId: "208359753148"
  },
  googleCloudVisionAPIKey: "AIzaSyDJ8KdQ3UGAlENfEA-c6vI9RCGXK1veWdQ",
  ttsAPIKey: "AIzaSyC4nySLQLh9mzIQmDg371K7kLkxPitUtM4",
  expressurl:"http://mtts.eastus.cloudapp.azure.com:3000/",
  soundoftexturl: "https://api.soundoftext.com/sounds/"
};
