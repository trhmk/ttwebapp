import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { environment } from '../../environments/environment';

@Injectable()
export class GoogleCloudVisionServiceProvider {

  constructor(public http: Http) { }

  // getLabels(base64Image) {
  //   const body = {
  //     "requests": [
  //       {
  //         "image": {
  //           "content": base64Image
  //         },
  //         "features": [
  //           {
  //             "type": "LABEL_DETECTION"
  //           }
  //         ]
  //       }
  //     ]
  //   }

  //   return this.http.post('https://vision.googleapis.com/v1/images:annotate?key=' + environment.googleCloudVisionAPIKey, body);
  // }

  getTexts(base64Image){
    //console.log('base64 image =>',_base64Image)
    let base64str: String = base64Image;
    const body = {
      "requests": [
        {
          "image": {
            "content": base64str
          },
        "features": [
          {
            "type": "DOCUMENT_TEXT_DETECTION"
          }
        ],
        "imageContext": {
        }
      }
    ]
}
   
   return this.http.post('https://vision.googleapis.com/v1/images:annotate?key=' + environment.googleCloudVisionAPIKey, body);
  }

}
