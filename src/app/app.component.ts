import { Component, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { GoogleCloudVisionServiceProvider } from '../providers/google-cloud-vision-service/google-cloud-vision-service';
//import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../environments/environment';
import { LoaderService } from '../providers/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
//@ViewChild('audioOption') audioPlayerRef: ElementRef

export class AppComponent {
  color = 'primary';
  mode = 'indeterminate';
  value = 50;
  bufferValue = 75;
  loading: boolean = false;
  title = 'app';
  req: ApiReq;
  res: any;
  req1: any;
  res1: any;
  url = environment.soundoftexturl;
  expressurl = environment.expressurl;

  fileurl="";
  ttsForm: FormGroup;
  _fileName: any;
  _file: any;
  result: any = "";
  resultText: any = "";
  textArr: any =[];
  audioArr: any = [];
  //audio: any;
 
  constructor (private fb: FormBuilder, private http: HttpClient, private sanitizer: DomSanitizer,
    public vision: GoogleCloudVisionServiceProvider) {
    this.createForm();
  }

  createForm () {
    this.ttsForm = this.fb.group({
      //apiKey: localStorage.getItem('apiKey'),
      apiKey: environment.ttsAPIKey,
      inputText: '',
      voiceName: [],
      speed: '1.00',
      pitch: '0.00'
    });
  }

  // onAudioPlay(){
  //   this.audioPlayerRef.nativeElement.play();
  // }
  
  onSubmit () {
    this.req = {
      audioConfig: {
        audioEncoding: 'LINEAR16',
        pitch: this.ttsForm.value.pitch,
        speakingRate: this.ttsForm.value.speed
    },
      input: {
        //text: this.ttsForm.value.inputText
        text: this.resultText
      },
      voice: {
         languageCode: 'en-US',
       //   languageCode: 'my',
        // name: 'en-US-Standard-A'
        //name: this.ttsForm.value.voiceName
        // name:'my-*'
      }
    };
    //localStorage.setItem('apiKey', this.ttsForm.value.apiKey);
    this.res = undefined;
    
    this.http.post('https://texttospeech.googleapis.com/v1beta1/text:synthesize?key=' + environment.ttsAPIKey, this.req, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .subscribe(data => {
        console.log(data);
        this.res = this.sanitizer.bypassSecurityTrustResourceUrl('data:audio/mp3;base64,' + data['audioContent']);

      });
  }

  async onSpeak () {
    let temp =""; 
    let range = 160;
    this.res = undefined;
    this.fileurl = "";
    this.audioArr = [];
    this.textArr = [];
    this.loading = true;
    // window.caches.delete(this.fileurl);
    // window.caches.delete(this.res);
    window.localStorage.clear();
    window.sessionStorage.clear();
    
    if(this.resultText.length == 0){
      window.alert("No text to speak!")
    }
    else{
      this.resultText += '\n\n';
      for(let i=0; i<this.resultText.length; i++){
        if(i < range && i< this.resultText.length - 1){
          temp += this.resultText.charAt(i);
        }
        else{
          while(this.resultText.charAt(i+1) !== ' ' && this.resultText.charAt(i+1) !== '\n' && this.resultText.charAt(i+1) !== '"' && this.resultText.charAt(i+1) !== '၊' && this.resultText.charAt(i+1) !== '။' &&  this.resultText.length != (i+1)){
            i++;
            temp += this.resultText.charAt(i);
          }
          if((this.resultText.length - i) <= 20 && this.resultText.length != (i+1)){
            while(i < this.resultText.length){
              i++;
              temp += this.resultText.charAt(i);
            }
          }
          //console.log(temp);
          await this.Speak(temp);
          this.textArr.push(temp);
          temp = "";
          range = i + 160;
          if(range > this.resultText.length){ range = this.resultText.length - 1; }
          //console.log(range);
        }
      }  
      
      let json = {
        "array": this.audioArr
      }
      
      this.fileurl = "";
      if(this.audioArr.length == 1){
        this.fileurl = this.audioArr[0];
        //this.res = this.sanitizer.bypassSecurityTrustResourceUrl(this.fileurl);
        let date = Date.now();
        let srcurl = this.fileurl + "?nocache=" + date.toString();
        this.res = this.sanitizer.bypassSecurityTrustResourceUrl(srcurl);
        this.loading = false;
      }
      else{
        await this.http.post(this.expressurl+'tts',json, {
          headers: {
            'Content-Type': 'application/json',
            
          }
        })
        .subscribe((response) => {
          //console.log(JSON.stringify(response));
          if(response['status'] == 200){
            this.fileurl = this.expressurl + 'static/merge.mp3';
            let date = Date.now();
            let srcurl = this.fileurl + "?nocache=" + date.toString();
            this.res = this.sanitizer.bypassSecurityTrustResourceUrl(srcurl);
            this.loading = false;
          }
        })
      }
    } //else    
  }

  // async onSpeak () {
  //      let fileurl = this.expressurl + 'static/merge.mp3';
  //      this.res = this.sanitizer.bypassSecurityTrustResourceUrl(fileurl);
  // }

  async Speak(splittext) {

    this.res = undefined;
    this.fileurl = "";

    this.req1 = {
      "engine": "Google",
      "data": {
        "text":splittext,
        "voice": "my"
      }
    };
   
    this.req1.data.text = splittext; 
     await this.http.post(this.url, this.req1, {
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'SoundOfTextClient',
        }
      })
      .toPromise().then(async data => {
        //console.log('sound data ', data);
        let soundlink = undefined;
          await this.http.get(this.url + data['id']).toPromise().then(async soundurl =>{
          //console.log('sound url => ', soundurl)
          console.log('soundlink ', soundurl['location']);
          //soundlink = soundurl['location'];
          if(soundurl['location'] != undefined){
            this.audioArr.push(soundurl['location']);
          }
          if(soundlink = soundurl['status'] != 'Done'){
            await this.http.get(this.url + data['id']).toPromise().then(soundurl =>{
              //console.log('sound url => ', soundurl)
              console.log('soundlink ', soundurl['location']);
              //soundlink = soundurl['location'];
              if(soundurl['location'] != undefined){
                this.audioArr.push(soundurl['location']);
              }
            });
          }
          
        })
      });
 }

  async getBase64(event) {
    let file = event.target.files[0];
    const filename = event.target.files[0].name;
    let extension = filename.substring(filename.lastIndexOf('.') + 1);
    if(extension.toLowerCase() == 'pdf'){
      this.loading = true;
      let uploadurl = this.expressurl + 'fileupload';
      const formData: FormData = new FormData();
      formData.append('file', file, file.name);
      await this.http.post(uploadurl, formData)
        .subscribe((response) => {
            //console.log('response received is ', response);
            let json = {
              "file":"./static/document.pdf"
              }
            this.http.post(this.expressurl+'tts',json, {
              headers: {
                'Content-Type': 'application/json',
              }
            })
            .subscribe((response) => {
              let res = JSON.stringify(response);
              let base64image: any = "";
              let res1 = JSON.parse(res);
              base64image = res1.base64image;
              let that = this;
              
              if(base64image != ""){
                //console.log(base64image);
                that.vision.getTexts(base64image).subscribe((res) => {
                  that.result = res.json().responses;
                // that.resultText = that.result[0].fullTextAnnotation.text;
                  that.resultText = that.result[0].textAnnotations[0].description;  
                  this.loading = false;    
                }, err => {
                  console.log(err);
                });
              }
              else{
                console.log('Error!');
              }
          })
        })
   }
   else if(extension.toLowerCase() == 'png' || extension.toLowerCase() == 'jpg' || extension.toLowerCase() == 'jpeg'){
          let that = this;
          let reader = new FileReader();
          let base64image: any = "";
          that.loading = true;
          reader.readAsDataURL(file);
          reader.onload = function () {
            base64image = reader.result;
            base64image = base64image.split(',')[1];
            if(base64image != ""){
              // console.log(base64image);
              that.vision.getTexts(base64image).subscribe((res) => {
                that.result = res.json().responses;
               // that.resultText = that.result[0].fullTextAnnotation.text;
                that.resultText = that.result[0].textAnnotations[0].description;  
                that.loading = false;
              }, err => {
                console.log(err);
              });
            }
            else{
              console.log('Error!');
            }
          };
          reader.onerror = function (error) {
            console.log('Error: ', error);
          };
       }
    else{
      console.log('File extension is not valid.')
    }
   }
   
   Clear(){
    let url = this.expressurl + 'file';
    this.resultText = "";
    this.fileurl = "";
    this.res = undefined;
    this.result = "";
    this.audioArr = [];
    this.textArr = [];
    this.loading = false;
    // this.audio = new Audio();
    // this.audio.src = "";
    // this.audio.load(); 
    
  //   this.http.delete(url).subscribe((response) => {
  //     if(response['status'] == 200){
  //       console.log('successful clear all');
  //     }
  //  });
    
    // window.caches.delete(this.fileurl);
    // window.caches.delete(this.res);
    window.localStorage.clear();
    window.sessionStorage.clear();

  }

 }


export interface ApiReq {

  audioConfig: {
    audioEncoding: string,
    pitch: string
    speakingRate: string

  };
  input: {
    text: string
  };
  voice: {
    languageCode: string,
    //name: string
  };

}

